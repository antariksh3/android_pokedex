package com.example.pokedex;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
    EditText etName , etRank ;
    RadioButton rbName , rbRank ;
    RadioGroup rgGroup ;
    Button btnSearch ;
    Boolean searchByName = true ;
    ScrollView scrollView;
    LinearLayout layoutSorry , layoutHi , layoutBase ;

    //pokemon stuff
    ImageView imgPokemon ;
    TextView tvPokemonName , tvPokemonType , tvRank , tvHeight , tvWeight , tvAbilities , tvMoves ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeView();
        hideShowInitialView();

        rgGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == rbName.getId()) {
                    etName.setVisibility(View.VISIBLE);
                    etRank.setVisibility(View.GONE);
                    searchByName = true ;
                }
                if (checkedId == rbRank.getId()) {
                    etRank.setVisibility(View.VISIBLE);
                    etName.setVisibility(View.GONE);
                    searchByName = false ;
                }
            }
        });
        btnSearch.setOnClickListener(this);
        rbRank.setOnClickListener(this);
        rbName.setOnClickListener(this);
    }

    public void initializeView() {
        etName = findViewById(R.id.et_name);
        etRank = findViewById(R.id.et_rank);
        rbName = findViewById(R.id.rb_name);
        rbRank = findViewById(R.id.rb_rank);
        rgGroup = findViewById(R.id.rg_name_rank);
        btnSearch = findViewById(R.id.btn_search);
        imgPokemon = (ImageView) findViewById(R.id.imgView);
        tvPokemonName = (TextView) findViewById(R.id.tv_pokemonName);
        tvPokemonType = (TextView) findViewById(R.id.tv_pokemonType);
        scrollView = findViewById(R.id.sv_scrollview);
        layoutHi = findViewById(R.id.layout_initial);
        layoutSorry = findViewById(R.id.layout_sorry);
        tvRank = findViewById(R.id.tv_rank);
        tvHeight = findViewById(R.id.tv_height);
        tvWeight = findViewById(R.id.tv_weight);
        tvAbilities = findViewById(R.id.tv_abilities_list);
        tvMoves = findViewById(R.id.tv_moves_list);
        layoutBase = findViewById(R.id.layout_base);
    }
    public void hideShowInitialView() {
        etName.setVisibility(View.VISIBLE);
        etRank.setVisibility(View.GONE);
        searchByName = true ;
        scrollView.setVisibility(View.GONE);
        layoutHi.setVisibility(View.VISIBLE);
        layoutSorry.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_search) {
            layoutHi.setVisibility(View.GONE);
            layoutSorry.setVisibility(View.GONE);
            if (searchByName) {
                if(checkIfTextIsPresent(etName)){
                    fetchPokemonInfo(etName);
                }
            } else {
                if(checkIfTextIsPresent(etRank)){
                    fetchPokemonInfo(etRank);
                }
            }
        }
    }

    public boolean checkIfTextIsPresent(EditText et) {
        if(et!= null && et.getText()!=null && et.getText().toString() != null &&
                !et.getText().toString().trim().equalsIgnoreCase("")) {
            return true ;
        }
        return false ;
    }

    public void fetchPokemonInfo(EditText et) {

        new JSONTask().execute(et.getText().toString());
    }

    public class JSONTask extends AsyncTask<String,String,String>{
        private final static String BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
        String imgUrl ;
        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection connection = null ;
            BufferedReader reader = null ;
            try {
                URL url = new URL(BASE_URL + params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                System.out.println("buffer -> "+buffer.toString());
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                if (reader!= null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null ;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("result -> "+result);

            try {
                JSONObject obj = new JSONObject(result);
                String name = obj.getString("name");

                String rank = obj.getString("id");
                JSONArray types = obj.getJSONArray("types");
                JSONObject typeObj = types.getJSONObject(0);
                JSONObject typeSubObj = typeObj.getJSONObject("type");
                String typeStr = typeSubObj.getString("name");

                JSONObject sprites = obj.getJSONObject("sprites");
                String imgUrl = sprites.getString("front_default");
                String imgUrl2 = sprites.getString("back_default");

                String height = obj.getString("height");
                String weight = obj.getString("weight");

                JSONArray arrAbilities = obj.getJSONArray("abilities");
                StringBuilder sbAbilities = new StringBuilder();
                for ( int i=0 ; i<arrAbilities.length() ; i++ ) {
                    JSONObject job = arrAbilities.getJSONObject(i);
                    JSONObject jobInner = job.getJSONObject("ability");
                    String ability = jobInner.getString("name");
                    sbAbilities.append(toInitCap(ability)).append("\n");
                }

                JSONArray arrMoves = obj.getJSONArray("moves");
                StringBuilder sbMoves = new StringBuilder();
                for ( int i=0 ; i<arrMoves.length() ; i++ ) {
                    JSONObject job = arrMoves.getJSONObject(i);
                    JSONObject jobInner = job.getJSONObject("move");
                    String move = jobInner.getString("name");
                    sbMoves.append(toInitCap(move)).append("\n");
                }

                int imgId = getResources().getIdentifier("com.example.pokedex:drawable/pok"+rank, null, null);
                imgPokemon.setImageResource(imgId);



                //imgPokemon.setImageBitmap(image);


                scrollView.setVisibility(View.VISIBLE);
                tvPokemonName.setText(toInitCap(name));
                tvPokemonType.setText("Type : "+toInitCap(typeStr));
                tvRank.setText("\nRank : "+rank);
                tvHeight.setText("\nHeight : "+height+" Feet(s)");
                tvWeight.setText("\nWeight : "+weight+" Kilograms");
                tvAbilities.setText(sbAbilities.toString());
                tvMoves.setText(sbMoves.toString());

                updateBackgroundColor(typeStr);

            } catch (JSONException | NullPointerException e) {
                scrollView.setVisibility(View.GONE);
                e.printStackTrace();
                System.out.println("SAD PIKACHU");
                layoutSorry.setVisibility(View.VISIBLE);
            }

        }

        public void updateBackgroundColor(String type) {
            switch(type){
                case "normal":
                    layoutBase.setBackgroundColor(Color.parseColor("#9EA674"));
                    break;
                case "fire":
                    layoutBase.setBackgroundColor(Color.parseColor("#F0812D"));
                    break;
                case "water":
                    layoutBase.setBackgroundColor(Color.parseColor("#6091F6"));
                    break;
                case "grass":
                    layoutBase.setBackgroundColor(Color.parseColor("#75C950"));
                    break;
                case "electric":
                    layoutBase.setBackgroundColor(Color.parseColor("#F7D72C"));
                    break;
                case "ice":
                    layoutBase.setBackgroundColor(Color.parseColor("#99D6D5"));
                    break;
                case "fighting":
                    layoutBase.setBackgroundColor(Color.parseColor("#8F2F30"));
                    break;
                case "poison":
                    layoutBase.setBackgroundColor(Color.parseColor("#9F4EA3"));
                    break;
                case "ground":
                    layoutBase.setBackgroundColor(Color.parseColor("#ECC361"));
                    break;
                case "flying":
                    layoutBase.setBackgroundColor(Color.parseColor("#AB94F8"));
                    break;
                case "psychic":
                    layoutBase.setBackgroundColor(Color.parseColor("#F1538D"));
                    break;
                case "bug":
                    layoutBase.setBackgroundColor(Color.parseColor("#ABB722"));
                    break;
                case "rock":
                    layoutBase.setBackgroundColor(Color.parseColor("#BB9E36"));
                    break;
                case "ghost":
                    layoutBase.setBackgroundColor(Color.parseColor("#7054A3"));
                    break;
                case "dragon":
                    layoutBase.setBackgroundColor(Color.parseColor("#7238FF"));
                    break;
                case "dark":
                    layoutBase.setBackgroundColor(Color.parseColor("#6B5A4A"));
                    break;
                case "steel":
                    layoutBase.setBackgroundColor(Color.parseColor("#B5B9D2"));
                    break;
                case "fairy":
                    layoutBase.setBackgroundColor(Color.parseColor("#FF99E4"));
                    break;
                default:

            }
        }

        public String toInitCap(String param) {
            if (param != null && param.length() > 0) {
                char[] charArray = param.toCharArray();
                charArray[0] = Character.toUpperCase(charArray[0]);
                // set capital letter to first position
                return new String(charArray);
                // return desired output
            } else {
                return "";
            }
        }
        public class JSONTask2 extends AsyncTask<String,String,String>{
            String imgUrl ;
            @Override
            protected String doInBackground(String... params) {

                HttpURLConnection connection = null ;
                BufferedReader reader = null ;
                try {
                    URL url = new URL(params[0]);
                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    return  bmp.toString();
                    //imageView.setImageBitmap(bmp);

                    // buffer.toString();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                    if (reader!= null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return null ;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                System.out.println("result -> "+result);

                try {
                    Bitmap bitmap = StringToBitMap(result);
                    imgPokemon.setImageBitmap(bitmap);
                } catch ( NullPointerException e) {
                    scrollView.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }
            public Bitmap StringToBitMap(String encodedString){
                try{
                    byte [] encodeByte = Base64.decode(encodedString,Base64.DEFAULT);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                    return bitmap;
                }
                catch(Exception e){
                    e.getMessage();
                    return null;
                }
            }
        }
    }
}

