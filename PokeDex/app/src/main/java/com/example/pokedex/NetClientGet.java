package com.example.pokedex;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

class NetClientGet {

    private final static String BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
    private String str ;
    public String getStr() {
        return this.str ;
    }
    public void setStr( String s ) {
        this.str = s ;
    }
    private String input ;
    public String getInput() {
        return this.input ;
    }
    public void setInput( String i ) {
        this.input = i ;
    }
    public String getResponse(String input) {
        setInput(input);
        String fullURL = BASE_URL + input ;


        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    try {
                        String fullURL = BASE_URL + getInput() ;
                        URL url = new URL(fullURL);
                        System.out.println("HITTTING : "+fullURL);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("Accept", "application/json");

                        if (conn.getResponseCode() != 200) {
                            throw new RuntimeException("Failed : HTTP error code : "
                                    + conn.getResponseCode());
                        }

                        BufferedReader br = new BufferedReader(new InputStreamReader(
                                (conn.getInputStream())));

                        String output;
                        System.out.println("Output from Server .... \n");
                        StringBuilder sb = new StringBuilder();
                        while ((output = br.readLine()) != null) {
                            //System.out.println(output);
                            sb.append(output);
                        }
                        setStr(sb.toString());
                        System.out.println("print: "+sb.toString());
                        conn.disconnect();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    //TODO
                    System.out.println("SAD PIKACHU1 : NO RES FOUND , CHECK INTERNET");
                    e.printStackTrace();
                }

            }
        });

        thread.start();

        String finalStr = getStr();
        if(finalStr!=null && finalStr.trim()!=""){
            return  finalStr;
        } else {
            //TODO
            System.out.println("SAD PIKACHU2 : NO RES FOUND , CHECK INTERNET");
            return null ;
        }

    }

}
